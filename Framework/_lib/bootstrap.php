<?php
@session_start();
include_once('share_lib/debug.lib.php');
include_once('share_lib/datastore.class.php');
include_once('share_lib/webpage.class.php');
include_once('share_lib/account_session.class.php');
include_once('share_lib/global.functions.php');
/*********************************************************************************
| Get the SQl Storage
*********************************************************************************/
// Determine if we wan to view the debugger
if(isset($_GET['request_string'])):
	$test = explode(".",$_GET['request_string']);
	if(reset(array_reverse($test)) == 'debugger'):
		ENABLE_DEBUGGING();
	die();
	endif;
endif;
if(isset($_SERVER['REQUEST_URI'])):
	$request_string = $_GET['request_string'];
	parse_str(parse_url($_SERVER['REQUEST_URI'], PHP_URL_QUERY), $_GET);
	$_GET['request_string'] = $request_string;
	if($_GET['request_string'] == "______REQUESTLATESTLOGFILE"):
		__LOADLATESTLOGFILE();
		die();
	endif;
endif;
$requestedPage = 'index';
$DataStore = new DataStoreClass();
if(isset($_SESSION['account']) && isset($_SESSION['account']['hash']) && isset($_SESSION['account']['username'])):
	$usr_salt = $_SESSION['account']['hash'];
	$usr_usernamel = $_SESSION['account']['username'];
	global $_ACCOUNT;
	$_ACCOUNT = new StartAccount($DataStore,trim($usr_usernamel),trim($usr_salt));
endif;
if(isset($_GET['request_string'])):
	$requestedPage = $_GET['request_string'];
	if(strtolower($requestedPage) == "_formhandler"):
		if(!isset($_POST['formlogic']) && isset($_GET['formlogic'])):
			unset($_GET['request_string']);
			$_POST = $_GET;
			unset($_GET);
		endif;
		include_once('_formhandler.php');
		die();
	endif;
endif;
$WebPage = new WebPage($requestedPage);
echo $WebPage->html($DataStore);
?>
