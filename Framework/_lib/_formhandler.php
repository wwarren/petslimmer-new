<?php
function createInput($input,$name = false){
	$return = "";
	if(is_array($input)):
		foreach($input as $subInputName => $subInput):
			if($name != false):
				$return .= createInput($subInput,$name.'['.$subInputName.']');
			else:
				$return .= createInput($subInput,$subInputName);
			endif;
		endforeach;
	else:
		if($name != false):
			$return .= "<input type='hidden' name='$name' value=\"".htmlentities($input)."\" />";
		else:
			$return .= "<input type='hidden' name='$name' value=\"".htmlentities($input)."\" />";
		endif;
	endif;
	return $return;
}
if(isset($_POST['formlogic'])):
	$formLogicFile = $_POST['formlogic'];
	unset($_POST['formlogic']);
	global $__defShow;
	$__defShow = true;
	if(is_file(SITE::$LIBPATH.'/share_lib/formhandlers/'.$formLogicFile.".formhandler.php")):
		$debug = false;
		$isXmlReq = isset($_SERVER['HTTP_X_REQUESTED_WITH']) ? ($_SERVER['HTTP_X_REQUESTED_WITH'] == "XMLHttpRequest" ? true : false ) : false;
		$redirect = $_SERVER['HTTP_REFERER'];
		$redirect_failure = $redirect;
		$redirect_post = array();
		$redirect_success = true;
		$redirect_referrer = "";
		include_once(SITE::$LIBPATH.'/share_lib/formhandlers/'.$formLogicFile.".formhandler.php");
		if($redirect_referrer != "" && reset(explode("?",$_SERVER['HTTP_REFERER'])) != SITE::$PATH."/".$redirect_referrer):
			$redirect_success = false;
			error('Unexpected Referrer, is {'.reset(explode("?",$_SERVER['HTTP_REFERER'])).'}, should be {'.SITE::$PATH."/".$redirect_referrer.'}');
		endif;
		if($isXmlReq):
			die();
		endif;
		if(count($redirect_post) == 0):
			if($debug === false):
				header("location: ".($redirect_success ? SITE::$PATH."/".$redirect : SITE::$PATH."/".$redirect_failure));
			die();
			endif;
		endif;
		?><form id='redirectform' method='post' action='<?php echo $redirect_success ? SITE::$PATH."/".$redirect : SITE::$PATH."/".$redirect_failure; ?>'  enctype="application/x-www-form-urlencoded">
		<?php
			echo createInput($redirect_post);
		?>
		</form><?php if($debug === false): ?><script type="text/javascript">
	    function redir () {
	        var frm = document.getElementById("redirectform");
	        frm.submit();
	    }
	    window.onload = redir;
		</script><?php endif;
	else:
		error('Formhandler '.$formLogicFile.' does not exist');
	endif;
else:
	header("location: 404");
endif;
?>
