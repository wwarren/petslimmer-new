<?php
/*
   ---------------------------------------
-- Class Name	: WebPage
-- File 		: webpage.class.php
-- Project		: Butlr.co.za
-- Date Created	: 2013/03/20
-- Created By 	: Rudi Versfeld
-- Type 		: Class
   ---------------------------------------
-- Description 	: Rendering Code for html web page
   ---------------------------------------
*/
class WebPage{
	/* Magic Methods */
		// Constructor
	public function __construct($template = ''){
		$template = $this->validateContentFile($template);
		$this->content_file = '/'.$template;
		// TODO
	}
	/* Variables */
	var $header_file = false;
	var $footer_file = false;
	var $content_file = null;
	var $page_name = '';
	var $root_page_name = '';
	/* Public Functions */
/*********************************************************************************
| Automation Stuff
*********************************************************************************/
	public function account_automation($SqlStore){
		if(account(true)):
			DEBUG("ACCOUNT AUTOMATION STARTED");
			DEBUG("ACCOUNT AUTOMATION ENDED");
		endif;
	}
/*********************************************************************************
| End of Automation Stuff
*********************************************************************************/
	private function validateContentFile($templateUrl){
		$fileParts = explode("/",$templateUrl);
		$fileParts = array_reverse($fileParts);
		$strlentorem = 0;
		$count = 0;
		$validUrl = false;
		if(file_exists(SITE::$TEMPLATEPATH."/".$templateUrl.'.php')):
			$validUrl = $templateUrl;
		elseif(file_exists(SITE::$TEMPLATEPATH."/".$templateUrl)):
			$validUrl = $templateUrl."/index.php";
		else:
			foreach($fileParts as $urlpart):
				if($validUrl == false):
					$count++;
					$strlentorem += strlen($urlpart)+1;
					if(file_exists(SITE::$TEMPLATEPATH."/".substr($templateUrl,0,(-1*$strlentorem)).'.php')):
						$validUrl = substr($templateUrl,0,(-1*$strlentorem));
					elseif(file_exists(SITE::$TEMPLATEPATH."/".substr($templateUrl,0,(-1*$strlentorem)))):
						$validUrl = substr($templateUrl,0,(-1*$strlentorem))."/index.php";
					endif;
				endif;
			endforeach;
		endif;
		$backAr = array_reverse(array_slice($fileParts,0,$count));
		$_REQUEST['URI'] = $backAr;
		if(substr($validUrl,0,1) == "/"):
			$validUrl = substr($validUrl,1);
		endif;
		if(substr($validUrl,-4,4) == ".php"):
			$validUrl = substr($validUrl,0,-4);
		endif;
		return $validUrl;
	}

	private function PrettyName($oldName){
		$nameParts = explode("/",$oldName);
		$returnName = "";
		foreach($nameParts as $part):
			$reverse = strrev( $returnName );
			// Echo or do whatever with it
			if( trim($part) != ""):
				if($reverse{0} == ""):
					if(strlen($returnName) == 0):
						 $returnName .= ucfirst(trim($part));
						 $this->root_page_name = ucfirst(trim($part));
					endif;
				else:
					$returnName .= " ".ucfirst(trim($part));
				endif;
			endif;
		endforeach;
		return $returnName;
	}

	public function html($DataStore){
		global $breadcrumb;
		ob_start();
		$this->page_name = $this->PrettyName($this->content_file);
		$this->account_automation($DataStore);
		if(file_exists(SITE::$TEMPLATEPATH.$this->content_file.'.php')):
			include(SITE::$TEMPLATEPATH.$this->content_file.'.php');
		elseif(file_exists(SITE::$TEMPLATEPATH.$this->content_file)):
			include(SITE::$TEMPLATEPATH.$this->content_file."/index.php");
		else:
			error("Template ".$this->content_file." was requested, could not be found in @ {".SITE::$TEMPLATEPATH.$this->content_file.'.php} OR {'.SITE::$TEMPLATEPATH.$this->content_file."/index.php".'}');
			include( SITE::$TEMPLATEPATH.'/error/404_page.php');
		endif;
		if($this->footer_file !== false):
			include(SITE::$TEMPLATEPATH.$this->footer_file);
		endif;
		$html = ob_get_contents();
		ob_end_clean();
		if($this->header_file !== false):
			ob_start();
			include(SITE::$TEMPLATEPATH.$this->header_file);
			$html = ob_get_contents().$html;
			ob_end_clean();
		endif;
		return $html;
	}
}
?>
