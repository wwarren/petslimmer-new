<?php
/*
   ---------------------------------------
-- Class Name	: Debug Class
-- File 		: debug.lib.php
-- Project		: Butlr.co.za
-- Date Created	: 2013/03/18
-- Created By 	: Rudi Versfeld
-- File Path 	: /Volumes/Macintosh HD/Users/rudi/Sites/butlr/share_lib
-- Type 		: Class
   ---------------------------------------
-- Description 	: Debugging Functions
   ---------------------------------------
*/
include_once('site.static.class.php');
ini_set('memory_limit', SITE::$MEMLIMIT);
global $__defShow;
$__defShow = false;
global $__DEBUG;
$__DEBUG = false;
function __LOADLATESTLOGFILE(){
		$files = glob(SITE::$LOGSPATH.'/*.log');
		$to_sort = array();

		foreach ($files as $filename)
		{
			$fileInfo = pathinfo($filename);
		   $to_sort[$fileInfo['filename']] = $filename;
		}

		krsort($to_sort);
		$lines = array_slice(file(reset($to_sort)), -100, 100, true);;
		foreach ($lines as $lineNum =>  $name) {
    	$decoded = json_decode(trim($name),true);
    	?>
    		<div class='request'>
    			<div class='logs'>
  					<div class='toolbar'>
  						<span>
  							INSPECT
  						</span>
  					</div>
    				<ul><li class='time-of-request'>
    					<?php echo date('l jS \of F Y @ h:i:s A',$decoded['TIMEOFREQUEST']); ?> to
    					<span class='requested_url'>
    						<?php echo $decoded['REQUESTEDURI']; ?>
    					</span> <?php echo  trim($decoded['REFERRER']) != '' ? "from
    					<span class='requested_url'>
    						".$decoded['REFERRER']."
    					</span>" : "" ; ?>.
    					<div class='stat notes'>
    						NOTES
    						<span>
    							<?php echo $decoded['NOTES']; ?>
    						</span>
    					</div>
    					<div class='stat debugs'>
    						DEBUGS
    						<span>
    							<?php echo $decoded['DEBUGS']; ?>
    						</span>
    					</div>
    					<div class='stat errors'>
    						ERRORS
    						<span>
    							<?php echo $decoded['ERRORS']; ?>
    						</span>
    					</div>
    				</li><?php
    					foreach($decoded['LOGS']['note'] as $note):
    						?>
    							<li class='note'>
    								<?php echo $note; ?>
    							</li>
    						<?php
    					endforeach;
    					foreach($decoded['LOGS']['debug'] as $debug):
    						?>
    							<li class='debug'>
    								<?php echo $debug; ?>
    							</li>
    						<?php
    					endforeach;
    					foreach($decoded['LOGS']['error'] as $error):
    						?>
    							<li class='error'>
    								<?php echo $error; ?>
    							</li>
    						<?php
    					endforeach;
    				?></ul>
    				MEMORY USAGE (<?php echo isset($decoded['MEMUSAGE']) ? $decoded['MEMUSAGE'] : ""; ?>)
    			</div>
    			<ul class='extras'>
    				<li>
    					SERVER
    					<?php
    						echoArrayUl($decoded['SERVER']);
    					?>
    				</li>
    				<li>
    					SESSION
    					<?php
    						echoArrayUl($decoded['SESSION']);
    					?>
    				</li>
    				<li>
    					POST
    					<?php
    						echoArrayUl($decoded['POST']);
    					?>
    				</li>
    				<li>
    					GET
    					<?php
    						echoArrayUl($decoded['GET']);
    					?>
    				</li>
    				<li>
    					FILES
    					<?php
    						echoArrayUl($decoded['FILES']);
    					?>
    				</li>
    			</ul>
    		</div>
    	<?php
    }
	exit(0);
}
function echoArrayUl($ar){
	echo "<ul>";
	foreach($ar as $name => $val):
		echo "<li>";
		echo "<span class='title'>".htmlentities($name)."</span>";
		if(is_array($val)):
			echoArrayUl($val);
		else:
			echo "<span class='value'>".htmlentities($val)."</span>";
		endif;
		echo "</li>";
	endforeach;
	echo "</ul>";
}

function ENABLE_DEBUGGING(){
	global $__DEBUG;
	$__DEBUG = true;
		echo '<script type="text/javascript" src="'.SITE::$PATH.'/js/jquery.js"></script>';
		echo "<script>";
		echo "
			function stayDowner(clicker){
				if(stayDown){
					$(clicker).css({
						color : '#EFEFEF',
						background : '#333'
					});
					stayDown = false;
					$(clicker).html('Stay Down');
				}else{
					$(clicker).css({
						color : '#333',
						background : '#EFEFEF'
					});
					stayDown = true;
					$(clicker).html('Staying Down');
				}
			}
			var stayDown = false;
			$(function(){
					window.setInterval(function(){
					  $('div.log').load('".SITE::$PATH."/______REQUESTLATESTLOGFILE');
					  if(typeof(stayDown) != undefined){
					  	if(stayDown){
					  		$('div.log').scrollTop($('div.log')[0].scrollHeight);
					  	}
					  }
					}, 1000);
					$(document).on('click','.toolbar',function(){

						var selected = $(this).parent().parent().clone();
						$('.inspector').html(selected);
						$('.inspector *').show();
						$('.inspector .toolbar').hide();
						$('ul.extras > li > ul').hide();
						$('ul.extras > li').click(function(){ $('> ul',$(this)).toggle();});
					});
		});";
		echo "</script>";
		echo "<style>";
			echo "body, html {
				font-family:Verdana;
				background-color:#333;
				color:#EFEFEF;
				font-size:12px;
			}
			div.log {
				height:400px;
				overflow:scroll;
			}
			div.log .request .logs ul li:not(.time-of-request) {
				display:none;
			}
			div.inspector {
				background:#000;
				padding:10px;
			}
			div.inspector ul.extras > li{
				display:table-cell;
				border:1px solid blue;
				padding:10px;
				background:#EFEFEF !important;
				color:#333 !important;
				font-weight:bold;
				cursor:pointer;
			}

			div.inspector ul.extras > li > ul {
				border-top:2px solid #333;
				font-weight:normal;
				padding:10px;
			}
			.request > .extras {
				display:none;
			}
			.request > .logs {
				display:block;
			}
			.requested_url {
				color:lightblue;
			}
			.toolbar {
				float:right;
				background:#EFEFEF;
				color:blue;
				padding:5px;
				font-weight:bold;
				cursor:pointer;
			}
			.request {
				border-bottom:2px solid #EFEFEF;
			}
			.request div {
				display:inline-block;
			}
			.errors, .error {
				color:red !important;
			}
			.notes, .note {
				color:yellow !important;
			}
			.debugs, .debug {
				color:orange !important;
			}
			.title {
				padding-right:20px;
			}
			.value {
				float:right;
			}
			";
		echo "</style>";
		?>
		<div onclick='stayDowner(this);' style='position:fixed;top:0px;padding:5px;font-weight:bold;background:#EFEFEF;color:#333;'>
			stayDown
		</div>
		<div class='log'>
		</div>
		<div class='inspector'>
		</div>
		<?php
		exit(0);
}
function DISABLE_DEBUGGING(){
	global $__DEBUG;
	$__DEBUG = false;
}
global $DEBUGHEADERS;
$DEBUGHEADERS = false;
global $DEBUGFOOTERS;
$DEBUGFOOTERS = false;
function requestHeaders(){
	$meto = '';
	return $meta;
}
global $__DEBUGAR;
$__DEBUGAR = array();
global $__debuggerStopped;
$__debuggerStopped = false;
function __WriteToLogFile($type = 'debug',$msg){
	global $DEBUGHEADERS;
	global $DEBUGFOOTERS;
	global $__DEBUGAR;
	global $__debuggerStopped;
	if($__debuggerStopped === false):
		if($DEBUGHEADERS === false):
			$__DEBUGAR['SERVER'] = $_SERVER;
			$__DEBUGAR['POST'] = $_POST;
			$__DEBUGAR['GET'] = $_GET;
			$__DEBUGAR['FILES'] = $_FILES;
			$__DEBUGAR['SESSION'] = $_SESSION;
			$__DEBUGAR['LOGS'] = array(
				"note" => array(),
				"debug" => array(),
				"error" => array()
			);
			$__DEBUGAR['NOTES'] = 0;
			$__DEBUGAR['ERRORS'] = 0;
			$__DEBUGAR['DEBUGS'] = 0;
			$__DEBUGAR['TIMEOFREQUEST'] = $_SERVER['REQUEST_TIME'];
			$__DEBUGAR['REQUESTEDURI'] = $_SERVER['REQUEST_URI'];
			$__DEBUGAR['REFERRER'] = $_SERVER['HTTP_REFERER'];
			$__DEBUGAR['MEMUSAGE'] = _debug_echo_memory_usage();
			$DEBUGHEADERS = true;
		endif;
		$__DEBUGAR['LOGS'][$type][] = $msg;
		$__DEBUGAR[strtoupper($type."s")]++;
	else:
		$__DEBUGAR['LOGS'][$type][] = $msg;
		$__DEBUGAR[strtoupper($type."s")]++;
		_debug_stop();
	endif;

}
function finishRequestLog(){
	$FileName = date('YmdH');
	file_put_contents(SITE::$LOGSPATH."/".$FileName.".log",$endofrequest,FILE_APPEND);
}

function DEBUG($msg){
	__WriteToLogFile('debug',$msg);
}
function NOTE($msg){
	__WriteToLogFile('note',$msg);
}
function ERROR($msg){
	__WriteToLogFile('error',$msg);
}
function _debug_echo_memory_usage() {
  $mem_usage = memory_get_peak_usage(true);
  $return = "";
  if ($mem_usage < 1024)
      $return .=   $mem_usage." bytes ";
  elseif ($mem_usage < 1048576)
      $return .=   round($mem_usage/1024,2)." kilobytes";
  else
      $return .=   round($mem_usage/1048576,2)." megabytes";
   return $return;
}
function _debug_stop(){
	global $__DEBUGAR;
	$FileName = date('YmdH');
	$__DEBUGER['memusage'] = _debug_echo_memory_usage();
	file_put_contents(SITE::$LOGSPATH."/".$FileName.".log",json_encode($__DEBUGAR).PHP_EOL,FILE_APPEND);
}
if(!isset($_debug_logs)):
	global $_debug_errors;
	$_debug_errors = array();
	global $_debug_notes;
	$_debug_notes = array();
	global $_debug_logs;
	$_debug_logs = array();
endif;
function _debug_stop_script_override(){
	$isError = false;
  if(is_null($error = error_get_last()) === false){
  switch($error['type']){
      case E_ERROR:
      case E_CORE_ERROR:
      case E_COMPILE_ERROR:
      case E_USER_ERROR:
          $isError = true;
          break;
      }
  }
  if ($isError){
 		$error = error_get_last();
    ERROR($error['file']." @ line ".$error['line'].PHP_EOL.$error['message']);//do whatever you need with it
    _debug_stop();
  }
  global $__debuggerStopped;
  $__debuggerStopped = true;
	return false;
}
function nullify($thing){
	return null;
}
function pre($pre,$return = false){
	$str = "<pre style='font-family:verdana;font-size:14px;color:#333;background-color:#efefef;margin:20px;padding:10px;border:1px solid #333;box-shadow:inset 0px 0px 3px #333, 0px 0px 2px #efefef '>";
	$str .=	print_r($pre,true);
	$str .= "</pre>";
	if(!$return):
		echo $str;
	else:
		return $str ;
	endif;
}
error_reporting(0);
ini_set('display_errors', E_NONE);
set_error_handler("nullify");
set_exception_handler("nullify");
register_shutdown_function('_debug_stop_script_override');
?>
