<?php 
$pet_add::{
	$pet = $args[0];
	$consumer_id = $args[1];
	$staff_id = $args[2];
	$target_weight = $args[3];
	
	$id = $DataStore->pet->pet_insert(
												$consumer_id,
												$pet['breed'],
												$staff_id,
												$pet['pet_name'],
												$pet['dob'],
												$target_weight*1000,
												date('Y-m-d H:i:s'),
												$pet['gender'],
												1,1,1,
												'enabled',
												date('Y-m-d H:i:s'),
												$staff_id
											);	
											
	return $id;
	
};

$pet_insert::"INSERT INTO pets (
									consumer_id,
									pet_breed_id,
									staff_member_id,
									name,
									birthdate,
									target_weight,
									date_created,
									gender,
									gift_midway,
									gift_target,
									gift_enroll,
									status,
									date_modified,
									modified_user
								) 
						VALUES (
									{1},{2},{3},'{4}','{5}',{6},'{7}','{8}',{9},{10},{11},'{12}','{13}',{14}
								)";

?>