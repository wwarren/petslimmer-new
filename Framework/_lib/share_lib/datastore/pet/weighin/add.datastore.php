<?php
$add::{
	$weights = $args[0];
	$pet_id = $args[1];
	$staff_id = $args[2];
	
	$DataStore->pet->weighin->insert_weighin(
												date('Y-m-d H:i:s'),
												$weights['weight']*1000,
												$weights['chest']*1000,
												$weights['waist']*1000,
												$pet_id,
												$staff_id
											);
};

$insert_weighin::"INSERT INTO pet_weighins (
												weighin_date,
												weight,
												chest,
												waist,
												pet_id,
												staff_member_id
											)
									values (
												'{1}','{2}','{3}','{4}',{5},{6}
											)";

?>