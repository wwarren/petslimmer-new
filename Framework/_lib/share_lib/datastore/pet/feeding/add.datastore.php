<?php
$add::{
	$feeding = $args[0];
	$pet_id = $args[1];
	$species = $args[2];
	
	$products = $DataStore->pet->feeding->get_feeding_values($species,$feeding['product'],$feeding['product_type']);
	
	$DataStore->pet->feeding->feeding_insert(
												$pet_id,
												$products['dry'],
												$feeding['breakfast_dry'],
												$feeding['supper_dry'],
												$products['wet'],
												$feeding['breakfast_wet'],
												$feeding['supper_wet'],
												$feeding['product']
											);
    
    $free_bag_email = $DataStore->pet->feeding->$get_free_bag_details($species,$feeding['product'],'dry');
    return $free_bag_email;
};

$feeding_insert::"INSERT INTO pet_feeding (
											pet_id,
											product_id_dry,
											breakfast_amount_dry,
											supper_amount_dry,
											product_id_wet,
											breakfast_amount_wet,
											supper_amount_wet,
											category
										  ) 
								   VALUES (
								   			{1},'{2}','{3}','{4}','{5}','{6}','{7}','{8}'
								   		  )";


$get_product_name::"SELECT product_name,product_size FROM products WHERE product_id = {1}";

$get_feeding_values::{

	$species = $args[0];
	$prod = $args[1];
	$type = $args[2];
	
	$feedings = array('feline' => array(
	    						'md' => array(
	    							'wet' => 10,
	    							'dry' => 4),
	    						'rd' => array(
	    							'wet' => 11,
	    							'dry' => 5),
	    						'wd' => array(
	    							'wet' => 12,
	    							'dry' => 6),
	    						'mw' => array(
	    							'wet' => 20,
	    							'dry' => 18),
	    						'mm' =>array(
	    							'wet' => 21,
	    							'dry' => 19),
	    						'mpw' => array(
	    							'dry' => 24,
	    							'wet' => 20),
	    						'mpm' => array(
	    							'dry' => 25)),
	    				  'canine' => array(
	    				  		'jd' => array(
	    							'dry' => 1),
	    						'rd' => array(
	    							'wet' => 8,
	    							'dry' => 2),
	    						'rdm' => array(
	    							'wet' => 8,
	    							'dry' => 13),
	    						'wd' => array(
	    							'wet' => 9,
	    							'dry' => 3),
	    						'mw' => array(
	    							'wet' => 16,
	    							'dry' => 14),
	    						'mm' => array(
	    							'wet' => 17,
	    							'dry' => 15),
	    						'mmm' =>array(
	    							'wet' => 17,
	    							'dry' => 23),
	    						'mwm' =>array(
	    							'wet'=> 16,
	    							'dry'=> 22),
	    						'mpw' => array(
	    							'dry' => 26,
	    							'wet' => 16),
	    						'mpm' => array(
	    							'dry' => 27)));
	    if($type == 'wet/dry'):
	    	$products = array();
	    	$products['dry'] = $feedings[$species][$prod]['dry'];
	    	$products['wet'] = $feedings[$species][$prod]['wet'];
	    	return $products;
	    else:
	    	$products[$type] = $feedings[$species][$prod][$type];
	    	return $products;
	    endif;
};

$get_free_bag_details::{
    $product = $DataStore->pet->feeding->get_feeding_values($args[0],$args[1],$args[2]);
    
};


?>