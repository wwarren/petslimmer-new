<?php
$login::{
	$type = $args[2];
	$table = '';
    $field = '';
	$url = '';
	switch($type):
		case 'admin':
			$table = 'users';
			$field = 'email_address';
			$url = 'administrator';
			break;
		case 'staff':
			$table = 'staff_members';
			$field = 'email';
			$url = 'customer';
			break;
		case 'consumer':
			$table = 'consumers';
			$field = 'username';
			break;
	endswitch;
	$result = $DataStore->auth->get_user_pass($table,$field,$args[0]);
	if(!empty($result)):
		$compare_hash = md5($args[1].$result[0]['password_salt']);
		if($compare_hash == $result[0]['password_hash']):
			//Set login session
			$_SESSION['logged_user'] = $result[0]['id'];
			$_SESSION['type'] = $type;
			$DataStore->auth->set_last_login($table,'id',date('Y-m-d H:i:s'),$result[0]['id']);
			return array('check' => true, 'url' => $url."/home");
		else:
			return array('check' => false, 'url' => $url."/login");
		endif;
	else:
		return array('check' => false, 'url' => $url."/login");
	endif;
	
	
	
};

$set_last_login::"update {1} set last_login = '{3}' where {2} = {4}";

$get_user_pass::"select id,password_hash, password_salt from {1} where {2} = '{3}'";

?>