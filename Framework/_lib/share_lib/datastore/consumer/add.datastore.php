<?php
$add::{

	$owner = $args[0];
	$username = $args[0]['last_name'];
	$username = str_replace(" ","",strtolower($username));
	$username = str_replace("-","",strtolower($username));
	$username = str_replace(".","",strtolower($username));
	
	$consumer_username = $DataStore->consumer->get_username($username,0);
	$password = $DataStore->consumer->generate_password();
	
	$id = 3;
	$id = $DataStore->consumer->insert_consumer_details($owner['title'],
														$owner['first_name'],
														$owner['last_name'],
														$owner['email'],
														$consumer_username,
														$owner['tel_home'],
														$owner['tel_mobile'],
														$owner['tel_office'],
														str_replace(","," ",$owner['physical_address_1']),
														str_replace(","," ",$owner['physical_address_2']),
														str_replace(","," ",$owner['physical_address_3']),
														$owner['physical_city'],
														$owner['physical_postal_code'],
														$owner['prefered_comms'],
														date('Y-m-d H:i:s'),
														$args[1],
														date('Y-m-d H:i:s'),
														$args[1],
														str_replace(","," ",$owner['postal_address_1']),
														str_replace(","," ",$owner['postal_address_2']),
														str_replace(","," ",$owner['postal_address_3']),
														$owner['postal_city'],
														$owner['postal_postal_code'],
														$password['hash'],
														$password['salt'],
														0,
														'enabled',
														$owner['physical_country'],
														$owner['postal_country'],
														$owner['tel_home_code'],
														$owner['tel_mobile_code'],
														$owner['tel_office_code'],
														''
														);
														
	return array($id,$password['password']);
	
};


$get_username::{
/**
* Generates a username from last_name unique in the DB by adding a digit
**/

	$username = $args[0];
	$i = $args[1];
	if($i != 0):
        $username = $username.$i;
    else:
        $username = $username;
    endif;
	$result = $DataStore->consumer->select_consumer_username($username);
	if(!empty($result)):
		$i++;
        return $DataStore->consumer->get_username($username,$i);
	else:
		return $username;
	endif;
	
};

$generate_password::{
	$password = mt_rand ( 10000000 , 99999999 );
	$passwordSalt = '';
    while (strlen($passwordSalt) < 10):
        $passwordSalt .= rand(0,9);
    endwhile;
    
    $newPasswordHash = md5(md5($password) . $passwordSalt);
	return array("password" => $password, "hash" => $newPasswordHash, "salt" => $passwordSalt);
};

$insert_consumer_details::"INSERT INTO consumers ( 
												   title,
												   first_name,
												   last_name,
												   email,
												   username,
												   phone_home,
												   phone_mobile,
												   phone_office,
												   address_physical_line1,
												   address_physical_line2,
												   address_physical_line3,
												   address_physical_city,
												   address_physical_code,
												   communication_preferred,
												   date_created,
												   created_staff_member_id,
												   date_modified,
												   modified_staff_member_id,
												   address_postal_line1,
												   address_postal_line2,
												   address_postal_line3,
												   address_postal_city,
												   address_postal_code,
												   password_hash,
												   password_salt,
												   policy_accepted,
												   status,
												   country,
												   country_postal,
												   phone_home_country,
												   phone_mobile_country,
												   phone_office_country,
												   last_login
												 )
                       			VALUES
                       	   ( '{1}' , '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', '{9}', '{10}', '{11}', '{12}', 
                       	     '{13}', '{14}', '{15}', {16}, '{17}', {18}, '{19}', '{20}', '{21}', '{22}', '{23}', '{24}', '{25}', {26}, '{27}', '{28}', '{29}', '{30}', '{31}','{32}', '{33}' );";

$select_consumer_username::"SELECT username FROM consumers WHERE username = '{1}' ";

?>