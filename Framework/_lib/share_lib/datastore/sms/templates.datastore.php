<?php

$templates::{


$sms_templates = array( 
				3 => array(
					'title' => '{CONSUMER_TITLE}',
					'first_name' => '{CONSUMER_FIRST_NAME}',
					'last_name' => '{CONSUMER_LAST_NAME}',
					'username' => '{CONSUMER_USERNAME}',
					'password' => '{CONSUMER_PASSWORD}'		
				)
			);

return $sms_templates[$args[0]];

};

$fill_template::{
	$template_id = $args[0];
	$content_id = $args[1];
	$extra_vars = $args[2];

	$vars = $DataStore->sms->templates($template_id);
	$template_content = $DataStore->sms->select_id($template_id);
	$msg = $template_content[0]['content'];
	
	
	$template_data = 'template_'.$args[0];
	$results = $DataStore->sms->$template_data($content_id);
	$data = $results[0];
	if(!empty($extra_vars)):
		foreach($extra_vars as $key => $value):
			$data[$key] = $value;
		endforeach;
	endif;
		
	foreach($vars as $var => $replace):
		$msg = str_replace($replace, $data[$var], $msg);
	endforeach;
	
	return($msg);

};

$template_3::"SELECT title, first_name, last_name, username FROM consumers WHERE id= {1}";

?>