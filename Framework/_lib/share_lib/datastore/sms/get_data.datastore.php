<?php
$get_data::{
	if(isset($args[0])):
		$id = $args[0];
		$result = $DataStore->sms->select_id($id);
		return $result;
	else:
		$result = $DataStore->sms->select_all();
		return $result;
	endif;


};

$update_template::{
	$data = $args[0];
	$DataStore->sms->update_template_statement($data['description'],$data['content'],date('Y-m-d H:i:s'), $_SESSION['logged_user'],$data['template_id']);

};

$select_id::"SELECT title, content, description FROM templates_sms WHERE id = {1}";

$select_all::"SELECT templates_sms.id, 
					 templates_sms.title,  
					 templates_sms.date_modified, 
					 users.email_address
			  FROM templates_sms 
			  LEFT JOIN users ON templates_sms.modified_user_id = users.id";

$update_template_statement::"Update templates_sms set description = '{1}', content = '{2}', date_modified = '{3}', modified_user_id = {4} WHERE id = {5}";			  
?>