<?php
/*
   ---------------------------------------
-- Class Name	: Butlr
-- File 		: butlr_session.class.php
-- Project		: Butlr.co.za
-- Date Created	: 2013/03/26
-- Created By 	: Rudi Versfeld
-- File Path 	: /Volumes/Macintosh HD/Users/rudi/Sites/butlr/share_lib
-- Type 		: Class
   ---------------------------------------
-- Description 	: Butler Employee Class
   ---------------------------------------
*/
class StartAccount{
	/* Variables */
	private $sql = null;
	private $id = null;
	private $details = null;
	private $rememberMe = false;
	public $loggedIn = false;
	/* Magic Methods */
		// Constructor
	public function __construct($DataStore,$username = false,$salt = false,$id = false){
		@session_start();
		$this->dataStore = $DataStore;
		if($username == false && $salt == false && $id !== false):
			$empl_data = $this->dataStore->accounts->Get_AccountById($id);
		else:
			$empl_data = reset($this->dataStore->accounts->Get_AccountByPassAndUsername(trim($username),trim($salt)));
		endif;
		if($empl_data !== false):
			$this->id = $empl_data['id'];
			$this->details = $empl_data;
			$this->login();
			$this->loggedIn = true;
		else:
			$this->logout();
		endif;
	}
		// Request Instance as String
	public function __toString(){
		return ""; // Return an Empty String
	}
		// Request Instance a Function
	public function __invoke(){
		return ""; // Return an Empty String
	}

	function get($attr){
		if($this->loggedIn):
			return $this->details[$attr];
		else:
			$this->logout();
		endif;
	}

	/* Public Functions */
	function logout(){
		global $_ACCOUNT;
		$_ACCOUNT = NULL;
		if(isset($_SESSION['account'])):
			unset($_SESSION['account']);
			unset($_SESSION);
			header("location: ".SITE::$PATH);
		endif;
	}

	function login(){
		$_SESSION['account']['hash'] = $this->details['PasswordSalted'];
		$_SESSION['account']['username'] = $this->details['Username'];
	}

	function error(){
		if(isset($_SESSION['account'])):
			unset($_SESSION['account']);
		endif;
		header('location: '.SITE::$PATH.'/400');
	}
}
?>
