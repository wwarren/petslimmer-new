<?php
/*
   ---------------------------------------
-- Class Name	: Site
-- File 		: site.static.class.php
-- Project		: petslimmer.co.za
-- Date Created	: 2013/03/20
-- Created By 	: Rudi Versfeld
-- Type 		: Class
   ---------------------------------------
-- Description 	:
   ---------------------------------------
*/

class SITE{
    public static $PATH = 'http://localhost/~williamwarren/TemplateSite/HostedFiles';
    public static $PATHWITHOUTDOMAIN = '~williamwarren/TemplateSite/HostedFiles';
	public static $UPLOADPATH = '/Users/williamwarren/Sites/TemplateSite/Framework/_uploads';
	public static $LOGSPATH = '/Users/williamwarren/Sites/TemplateSite/Framework/_lib/logs';
	public static $FORMPATH = '/Users/williamwarren/Sites/TemplateSite/Framework/_lib/share_lib/_formhandler';
	public static $LIBPATH = '/Users/williamwarren/Sites/TemplateSite/Framework/_lib';
	public static $TEMPLATEPATH = '/Users/williamwarren/Sites/TemplateSite/Framework/_page_templates';
	public static $PLUGINPATH = '/Users/williamwarren/Sites/TemplateSite/Framework/_lib/share_lib/thirdparty/';
	public static $SITENAME = 'TemplateSite';
	public static $DBS = array(
		"dev" => array(
			"server" => "127.0.0.1",
			"user" => "root",
			"password" => "",
			"database" => "rework"
		)
	);
	public static $DbToUse = "dev";
	public static $MEMLIMIT = "112M";
    public static $DYNAMICSERVERIP = false;
}

/* Enable Dynamic Server Ip Setup */
if(SITE::$DYNAMICSERVERIP):
    $DYNAMIC_IP = $_SERVER['SERVER_ADDR'] === "::1" ? "localhost" : $_SERVER['SERVER_ADDR'];
    $ADD_PREFIX = "http://";
    $NEW_ADDR = $ADD_PREFIX.$DYNAMIC_IP.SITE::$PATHWITHOUTDOMAIN;
    SITE::$PATH = $NEW_ADDR;
endif;

?>
