<?php
/*
   ---------------------------------------
-- Class Name	: Blinkidee SqlStore
-- File 		: butlr.datastore.class.php
-- Project		: Butlr.co.za
-- Date Created	: 2013/03/18
-- Created By 	: Rudi Versfeld
-- File Path 	: /Volumes/Macintosh HD/Users/rudi/Sites/butlr/share_lib
-- Type 		: Class
   ---------------------------------------
-- Description 	: Its a Data Storage Class...
   ---------------------------------------
*/
class substore {
	function __construct(&$Store,$MyName,$MyFile){
		$this->StoreRef = $Store;
		$this->getSubStores($MyFile);
	}

	function __get($varName){
		if(isset($this->__DataStore[$varName])):
			debug('Pulling Requested Store {'.$varName.'}');
			if(is_object($this->__DataStore[$varName])):
				return $this->__DataStore[$varName];
			else:
				return $this->__DataStore[$varName];
			endif;
		else:
			$func = $this->$varName;
			return $func;
		endif;
	}

	public function __call($varName,$args){
		if($this->$varName instanceof Closure):
			$func =  $this->$varName;
			return $func($this->StoreRef,$args);
		else:
			$str = $this->$varName;
			debug('Pulling Requested Store {'.$varName.'}');
			if(is_object($str)):
				foreach($args as $argIndex => $argValue):
					$argValue = mysqli_escape_string($this->_conn,$argValue);
					$args[$argIndex] = $argValue;
				endforeach;
				if(is_array($this->__DataStore[$varName])):
					return $this->__DataStore[$varName];
				else:
					$str = $this->__DataStore[$varName]($this,$args);
					return $str;
				endif;
			else:
				foreach($args as $argIndex => $argValue):
						$argIndex = $argIndex+1;
						if($this->StoreRef->disableEscaping):
							$str = str_replace("{{$argIndex}}",stripslashes($argValue),$str);
						else:
							$str = str_replace("{{$argIndex}}",mysqli_escape_string($this->StoreRef->_conn,stripslashes($argValue)),$str);
						endif;
				endforeach;
				debug("Query Running : ".$str);
				return $this->StoreRef->query($str);
			endif;
		endif;
	}

	private function getSubStores($myDir){
		$dir = $myDir;
		$pattern = "*";
		$files = glob($dir . "/" . $pattern,GLOB_ONLYDIR);
		foreach($files as $storeFile):
	    	$subStoreName = str_replace($myDir,"",$storeFile);
	    	if(isset($this->__DataStore[$subStoreName])):
	    		die('SubStore is trying to override other main store function named '.$subStoreName);
	    	else:
	    		$this->__DataStore[str_replace("/","",$subStoreName)] = new substore($this->StoreRef,$subStoreName,$storeFile);
	    		$this->getSubStoreStores($myDir,$subStoreName);
	    	endif;
	    endforeach;
	}

	private function getSubStoreStores($myDir,$subStoreName){
		$dir = $myDir.$subStoreName."/";
		$pattern = "*.datastore.php";
		$files = glob($dir . $pattern);
	    foreach($files as $storeFile):
	    	$this->IncludeSubStoreFile($subStoreName,$storeFile);
	    endforeach;
	}

	private function IncludeSubStoreFile($__storeName, $__file){
			$contents = str_replace('::{',' = function($DataStore,$args){',file_get_contents($__file));
			$contents = str_replace('::"',' = "',$contents);
			$contents = str_replace("::'"," = '",$contents);
			$contents = str_replace(array("<?php","?>"),'',$contents);
			ob_start();
			eval($contents);
			ob_end_flush();
			$store = get_defined_vars();
			unset($store['contents']);
			unset($store['__file']);
			unset($store['__storeName']);
			foreach($store as $funcName => $func):
				$this->__DataStore[str_replace("/","",$__storeName)]->$funcName = $func;
			endforeach;
	}
	public $__DataStore = array();
};
class DataStoreClass {
	public $_conn = null;
	public $dieQuery = false;
	function __construct(){
		debug("Connecting to ".SITE::$DbToUse);
		$this->_conn = mysqli_connect(SITE::$DBS[SITE::$DbToUse]['server'],SITE::$DBS[SITE::$DbToUse]['user'],SITE::$DBS[SITE::$DbToUse]['password'],SITE::$DBS[SITE::$DbToUse]['database']);
		if (mysqli_connect_errno()) :
			error("Mysql Connection Error : ".mysqli_connect_error());
			exit(0);
		else:
			debug('Mysql Conn to '.SITE::$DBS[SITE::$DbToUse]['server'].' Open');
		endif;
		$this->getStores();
	}

	public $disableEscaping = false;

	private function getStores(){
		$dir = SITE::$LIBPATH."/share_lib/datastore/";
		$pattern = "*.datastore.php";
		$files = glob($dir . $pattern);
	    foreach($files as $storeFile):
	    	$this->IncludeStoreFile($storeFile);
	    endforeach;
	    $this->getSubStores();
	}

	private function getSubStores(){
		$dir = SITE::$LIBPATH."/share_lib/datastore/";
		$pattern = "*";
		$files = glob($dir . $pattern,GLOB_ONLYDIR);
	    foreach($files as $storeFile):
	    	$subStoreName = str_replace(SITE::$LIBPATH."/share_lib/datastore/","",$storeFile);
	    	if(isset($this->__DataStore[$subStoreName])):
	    		die('SubStore is trying to override other main store function named '.$subStoreName);
	    	else:
	    		$this->__DataStore[$subStoreName] = new substore($this,$subStoreName,$storeFile);
	    		$this->getSubStoreStores($subStoreName);
	    	endif;
	    endforeach;
	}

	private function getSubStoreStores($subStoreName){
		$dir = SITE::$LIBPATH."/share_lib/datastore/".$subStoreName."/";
		$pattern = "*.datastore.php";
		$files = glob($dir . $pattern);
	    foreach($files as $storeFile):
	    	$this->IncludeSubStoreFile($subStoreName,$storeFile);
	    endforeach;
	}

	private function IncludeSubStoreFile($__storeName, $__file){
		$contents = str_replace('::{',' = function($DataStore,$args){',file_get_contents($__file));
		$contents = str_replace('::"',' = "',$contents);
		$contents = str_replace("::'"," = '",$contents);
		$contents = str_replace(array("<?php","?>"),'',$contents);
		ob_start();
		eval($contents);
		ob_end_flush();
		$store = get_defined_vars();
		unset($store['contents']);
		unset($store['__file']);
		unset($store['__storeName']);
		foreach($store as $funcName => $func):
			$this->__DataStore[$__storeName]->$funcName = $func;
		endforeach;
	}

	private function IncludeStoreFile($__file){
		$contents = str_replace('::{',' = function($DataStore,$args){',file_get_contents($__file));
		$contents = str_replace('::"',' = "',$contents);
		$contents = str_replace("::'"," = '",$contents);
		$contents = str_replace(array("<?php","?>"),'',$contents);
		ob_start();
		eval($contents);
		ob_end_flush();
		$store = get_defined_vars();
		unset($store['contents']);
		unset($store['__file']);
		unset($store['__storeName']);
		$this->__DataStore = array_merge($this->__DataStore,$store);
	}

	function seeStore(){
		$note = "<h2>Available SQL requests</h2>";
		foreach($this->__DataStore as $name => $sql):
			$note .= "<h3>$name</h3>".$this->__DataStore[$name];
		endforeach;
		note($note);
	}

	public function getLastId(){
		return $this->_conn->insert_id;
	}

	public function query($sql){
		return $this->__query($sql);
	}

	private function __query($sql){
		$Qtype = reset(explode(" ",trim(str_replace(PHP_EOL," ",strtolower($sql)))));
		if($this->dieQuery):
			die($sql);
		endif;
		$resultSet = $this->_conn->query($sql);
    if($this->_conn->error):
		    error('SQL Error : <br />'.$this->_conn->error.'<div style="width:500px;font-size:80%;padding:10px;background:#efefef;color:#333;margin:5px;">'.$sql."</div>");
	  endif;
		$ReturnResult = array();
		if($Qtype == 'select'):
      while($row = $resultSet->fetch_assoc()):
      	$ReturnResult[] = $row;
      endwhile;
	  elseif($Qtype == 'insert'):
			return $this->getLastId();
	  endif;
    return $ReturnResult;
	}

	function __destruct(){
		debug('Closing Mysql Conn');
		mysqli_close($this->_conn);
	}

	function __get($varName){

		if(isset($this->__DataStore[$varName])):
			debug('Pulling Requested Store {'.$varName.'}');
			if(is_object($this->__DataStore[$varName])):
				return $this->__DataStore[$varName];
			else:
				return $this->__DataStore[$varName];
			endif;
		else:
			error('Requested Store {'.$varName.'} Does Not Exist');
			$this->seeStore();
		endif;
	}

	public function __call($varName,$args){
		$str = $this->$varName;
		debug('Pulling Requested Store {'.$varName.'}');
		if(is_object($str)):
			foreach($args as $argIndex => $argValue):
				//$argValue = mysqli_escape_string($this->_conn,$argValue);
				$args[$argIndex] = $argValue;
			endforeach;
			if(is_array($this->__DataStore[$varName])):
				return $this->__DataStore[$varName];
			else:
				$str = $this->__DataStore[$varName]($this,$args);
				return $str;
			endif;
		else:
			foreach($args as $argIndex => $argValue):
				$argIndex = $argIndex+1;
				if($this->disableEscaping):
					$str = str_replace("{{$argIndex}}",stripslashes($argValue),$str);
				else:
					$str = str_replace("{{$argIndex}}",mysqli_escape_string($this->_conn,stripslashes($argValue)),$str);
				endif;
			endforeach;
			debug("Query Running : ".$str);
			return $this->__query($str);
		endif;

	}

	public function escape($argValue){
		$escapeVal = mysqli_escape_string($this->_conn,$argValue);
		return $escapeVal;
	}

	public $__DataStore = array();
}
?>
