<?php
$debug = false; // set to true to enable debugger mode
$redirect = "successurl"; // url for success
$redirect_failure = "failureurl"; // url for failure, if empty then will always go to $redirect value
$redirect_post = array(); // values to send back to redirect script
$redirect_referrer = false; // false if not using
$redirect_success = true; //set to false to use failure redirect

$type = $_POST['type'];
$_SESSION = array();
if($type == 'staff'):
	$redirect = 'customer/login';
elseif($type == 'admin'):
	$redirect = 'administrator/login';
endif;
?>