<?php
$debug = false; // set to true to enable debugger mode
$redirect = "successurl"; // url for success
$redirect_failure = "failureurl"; // url for failure, if empty then will always go to $redirect value
$redirect_post = array(); // values to send back to redirect script
$redirect_referrer = false; // false if not using
$redirect_success = true; //set to false to use failure redirect

$check = $DataStore->auth->login($_POST['username'],$_POST['password'],$_POST['type']);
if($check['check']):
	$redirect = $check['url'];
else:
	$redirect_success = false;
	$redirect_failure = $check['url'];
	$redirect_post = array('error' => 'The details entered were incorrect');
endif;
?>
