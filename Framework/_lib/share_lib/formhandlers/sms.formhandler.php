<?php

$debug = false; // set to true to enable debugger mode
$redirect = "successurl"; // url for success
$redirect_failure = "failureurl"; // url for failure, if empty then will always go to $redirect value
$redirect_post = array(); // values to send back to redirect script
$redirect_referrer = false; // false if not using
$redirect_success = true; //set to false to use failure redirect

if($_POST['submit'] == 'Cancel'):
	$redirect = "administrator/content/sms_list";
elseif($_POST['submit'] == 'Save'):
	$DataStore->sms->update_template($_POST);
	$redirect_post = array("msg" => "Template successfully saved");
	$redirect = "administrator/content/sms_list";
elseif($_POST['submit'] == 'Send'):
	$DataStore->sms->send_sms($_POST['cellnum'],$_POST['country'],$_POST['content']);
	$redirect = "administrator/content/sms/".$_POST['template_id'];
endif;

?>
