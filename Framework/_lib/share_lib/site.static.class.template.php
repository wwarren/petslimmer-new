<?php
/*
   ---------------------------------------
-- Class Name	: Site
-- File 		: site.static.class.php
-- Project		: Butlr.co.za
-- Date Created	: 2013/03/20
-- Created By 	: Rudi Versfeld
-- Type 		: Class
   ---------------------------------------
-- Description 	:
   ---------------------------------------
*/

class SITE{
/*	public static $PATH = 'http://localhost/ProjectIdea/HostedFiles';
	public static $PATH = '/ProjectIdea/HostedFiles';
	public static $PATH = 'http://localhost/ProjectIdea/HostedFiles';
	public static $UPLOADPATH = 'c:/wamp/www/ProjectIdea/Framework/_uploads';
	public static $LOGSPATH = 'c:/wamp/www/ProjectIdea/Framework/_lib/logs';
	public static $FORMPATH = 'c:/wamp/www/ProjectIdea/Framework/_lib/share_lib/_formhandler';
	public static $LIBPATH = 'c:/wamp/www/ProjectIdea/Framework/_lib';
	public static $TEMPLATEPATH = 'c:/wamp/www/ProjectIdea/Framework/_page_templates';
	public static $SITENAME = 'blinkidee'; */
	public static $PATHWITHOUTDOMAIN = '/ProjectIdeas/HostedFiles';
	public static $PATH = 'http://localhost/ProjectIdeas/HostedFiles';
	public static $UPLOADPATH = '/var/www/html/ProjectIdeas/Framework/_uploads';
	public static $LOGSPATH = '/var/www/html/ProjectIdeas/Framework/_lib/logs';
	public static $FORMPATH = '/var/www/html/ProjectIdeas/Framework/_lib/share_lib/_formhandler';
	public static $LIBPATH = '/var/www/html/ProjectIdeas/Framework/_lib';
	public static $TEMPLATEPATH = '/var/www/html/ProjectIdeas/Framework/_page_templates'; */
	public static $SITENAME = 'blinkidee';
	public static $DBS = array(
		"dev" => array(
			"server" => "127.0.0.1",
			"user" => "root",
			"password" => "",
			"database" => "blinkidee"
		)
	);
	public static $DbToUse = "dev";
	public static $MEMLIMIT = "112M";
    public static $DYNAMICSERVERIP = true;
}

/* Enable Dynamic Server Ip Setup */
if(SITE::$DYNAMICSERVERIP):
    $DYNAMIC_IP = $_SERVER['SERVER_ADDR'];
    $ADD_PREFIX = "http://";
    $NEW_ADDR = $ADD_PREFIX.$DYNAMIC_IP.SITE::$PATHWITHOUTDOMAIN;
    SITE::$PATH = $NEW_ADDR;
endif;

?>
