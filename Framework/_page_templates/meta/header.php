<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title></title>
    <link rel="stylesheet" href="<?php echo SITE::$PATH; ?>/css/main.css" />
	<script type="text/javascript" src="<?php echo SITE::$PATH; ?>/js/jquery.js"></script>
	<script type="text/javascript" src="<?php echo SITE::$PATH; ?>/js/jquery.tablesorter.js"></script>
	<script type="text/javascript" src="<?php echo SITE::$PATH; ?>/js/md5.js"></script>
	<script type="text/javascript" src="<?php echo SITE::$PATH; ?>/js/jquery.html5form-1.5-min.js"></script>
	<script type="text/javascript" src="<?php echo SITE::$PATH; ?>/js/axl_system.js"></script>
	<script type="text/javascript" src="<?php echo SITE::$PATH; ?>/js/main.js"></script>    
	
  </head>
  <body class="antialiased" style='height:100%'>
      <input type='hidden' value='<?php echo SITE::$PATH; ?>' id='SITEROOT' />
