<?php include(SITE::$TEMPLATEPATH."/meta/header.php");  
	if(!$DataStore->auth->auth('staff')):
		header('location:'.SITE::$PATH.'/customer/login');
	endif;
	
	$pet_breeds = $DataStore->pet_breed->get_data();
	
?>
<form action="pet_add">
	<div id="select_consumer">
		New Consumer <br/>
		Existing Consumer
	</div>
	
	<div id="consumer">
		<h4>Pet Owner Details : </h4>
		
		<div class="column">
			
			<label for="title">Title</label> <br/><select name="owner[title]" >
				<option value="Mr">Mr</option>
				<option value="Mrs">Mrs</option>
				<option value="Ms">Ms</option>
				<option value="Dr">Dr</option>
				<option value="Prof">Prof</option> 
			</select><br/>
			<label for="first_name">First Name :</label> <br/><input name="owner[first_name]" type="text" /> <br/>
			<label for="last_name">Last Name :</label> <br/><input name="owner[last_name]" type="text" /> <br/>
			<label for="email">Email Address :</label> <br/><input name="owner[email]" type="text" /> <br/>
		</div>
		<div class="column">	
			<label for="tel_mobile">Telephone Number(Mobile) : </label> <br />
			<select name="owner[tel_mobile_code]">
					<option value="rsa">RSA</option>
					<option value="nam">NAM</option>
			</select>
			<input name="owner[tel_mobile]" type="text" /> <br/>
			<label for="tel_home">Telephone Number(Home) : </label> <br />
			<select name="owner[tel_home_code]">
					<option value="rsa">RSA</option>
					<option value="nam">NAM</option>
			</select>
			<input name="owner[tel_home]" type="text" /> <br/>
			<label for="tel_office">Telephone Number(Office) : </label> <br />
			<select name="owner[tel_office_code]">
					<option value="rsa">RSA</option>
					<option value="nam">NAM</option>
			</select>
			<input name="owner[tel_office]" type="text" /> <br/>
			<label>Prefered Method Of Communication</label>
			<select name="owner[prefered_comms]">
				<option value="sms">SMS</option>
				<option value="email">Email</option>
				<option value="postage">Postage</option>
			</select><br/>
		</div>
		<div style="clear:left;">
		<div class="column">
			<label for="postal_address_1">Postal Address</label> <br/>
			<input type="text" name="owner[postal_address_1]" /> <br/>
			<input type="text" name="owner[postal_address_2]" /> <br/>
			<input type="text" name="owner[postal_address_3]" /> <br/>
			
			<label for="postal_city">City</label><br/>
			<input type="text" name="owner[postal_city]" /><br/>
			
			<label for="postal_postal_code">Postal Code</label><br/>
			<input type="text" name="owner[postal_postal_code]" /><br/>
			
			<label for="postal_country">Country</label><br/>
			<select name="owner[postal_country]">
				<option value="rsa">South Africa</option>
				<option value="nam">Namibia</option>
			</select><br/>
		</div>
		<div class="column">
			<label for="physical_address_1">Physical Address</label> <br/>
			<input type="text" name="owner[physical_address_1]" /> <br/>
			<input type="text" name="owner[physical_address_2]" /> <br/>
			<input type="text" name="owner[physical_address_3]" /> <br/>
			
			<label for="physical_city">City</label><br/>
			<input type="text" name="owner[physical_city]" /><br/>
			
			<label for="physical_postal_code">Postal Code</label><br/>
			<input type="text" name="owner[physical_postal_code]" /><br/>
			
			<label for="physical_country">Country</label><br/>
			<select name="owner[physical_country]">
				<option value="rsa">South Africa</option>
				<option value="nam">Namibia</option>
			</select><br/>
		</div>
	</div>
	<div id="pet_details">
		<h4>Pet Details : </h4>
		<div class="column">
			<label for="species">Species</label> <br/>
			<select id="species" name="pet[species]">
				<option value="canine">Dog</option>
				<option value="feline">Cat</option>
			</select> <br/>
			<label for="pet_name">Name</label> <br/>
			<input type="text" name="pet[pet_name]" /> <br/>
			
			<label for="dob">Date of Birth</label> <br/>
			<input type="text" name="pet[dob]" /> <br/>
			
			<label for="breed">Breed</label><br/>
			<select name="pet[breed]">
				<option value="canine" disabled="disabled">Canine</option>
					<?php foreach($pet_breeds['canine'] as $breed): ?>
					<option class="breed_<?php echo $breed['species']; ?>" value=<?php echo $breed['id']; ?> ><?php echo $breed['breed_name']; ?> </option>
					<?php endforeach; ?>
				<option value="feline" disabled="disabled">Feline</option>
					<?php foreach($pet_breeds['feline'] as $breed): ?>
					<option class="breed_<?php echo $breed['species']; ?>" value=<?php echo $breed['id']; ?> ><?php echo $breed['breed_name']; ?> </option>
					<?php endforeach; ?>	
			</select> <br/>
			
			<label for="gender">Gender</label> <br/>
			<select name="pet[gender]">
				<option value="male">Male</option>
				<option value="female">Female</option>
			</select><br/>
			
			<label for="starting_weight">Starting Weight</label> <br/>
			<input type="text" value="000.00" name="pet[starting_weight]" /> <br/>
			
			<label for="starting_chest">Starting Chest Measurement(cm)</label> <br/>
			<input type="text" value="000.00" name="pet[starting_chest]" /> <br/>
			
			<label for="starting_waist">Starting Waist Measurement(cm)</label> <br/>
			<input type="text" value="000.00" name="pet[starting_waist]" /> <br/>

			<div> 
			      <p>Please Select BFI index </p><label style="float:right; display:inline;"><a href='#' >BFI Tool</a></label><br />
			      <input id="bfi" class="validate[required] bfi-rad" type="radio" name="pet[bfi]" value="20" <?php //if($pets['bfi'][$i] == 20){echo 'checked="checked"';} ?>
/> 
			      <p>16-25% body fat, <span style="color:#15A442;"><b>LOW RISK</b></span></p><br />
			      <input id="bfi" class="validate[required] bfi-rad" type="radio" name="pet[bfi]" value="30" <?php //if($pets['bfi'][$i] == 30){echo 'checked="checked"';} ?> /> 
			      <p>26-35% body fat, <span style="color:#FEC60A;"><b>MILD RISK</b></span></p><br />
			      <input id="bfi" class="validate[required] bfi-rad" type="radio" name="pet[bfi]" value="40" <?php //if($pets['bfi'][$i] == 40){echo 'checked="checked"';} ?> /> 
			      <p>36-45% body fat, <span style="color:#FD9308;"><b>MODERATE RISK</b></span></p><br />
			      <input id="bfi" class="validate[required] bfi-rad" type="radio" name="pet[bfi]" value="50" <?php //if($pets['bfi'][$i] == 50){echo 'checked="checked"';} ?> /> 
			      <p>46-55% body fat, <span style="color:#FC4B07;"><b>SERIOUS RISK</b></span></p><br />
			      <input id="bfi" class="validate[required] bfi-rad" type="radio" name="pet[bfi]" value="60" <?php //if($pets['bfi'][$i] == 60){echo 'checked="checked"';} ?> /> 
			      <p>56-65% body fat <span style="color:#FC0007;"><b>SEVERE RISK</b></span></p><br />
			      <input id="bfi" class="validate[required] bfi-rad" type="radio" name="pet[bfi]" value="70" <?php //if($pets['bfi'][$i] == 70){echo 'checked="checked"';} ?> /> 
			      <p>>65% body fat, <span style="color:#D10009;"><b>EXTREME RISK</b></span></p><br />
		    </div>
		    <label for="medical_notes">Medical Condition / Notes</label><br/>
		    <textarea name="pet[medical_notes]"></textarea><br/>
		</div>
		<h4>Feeding Details</h4>
		<div class="column">
			<label for="target_weight">Target Weight(Kg)</label> <br/>
			<input type="text" value="000.00" name="feeding[target_weight]" /> <br/>
			
			<label for="product">Recommended Product</label> <br/>
			<select name="feeding[product]">
				<option value="mw">Metabolic Weight loss</option>
			</select><br/>
			
		    <label>Dry / Wet<span class="required">*</span></label><br/>
		    Dry<input id="0rad1" type="radio" name="feeding[product_type]" value="dry" checked="">
		    Wet<input id="0rad2" type="radio" name="feeding[product_type]" value="wet">
		    Dry &amp; Wet<input id="0rad3" type="radio" name="feeding[product_type]" value="wet/dry">
		    <br/>
		    
		    Recommended Feeding Guidelines<br/>
		    <label>Breakfast</label>Dry<input id="breakfast_dry" type="text" name="feeding[breakfast_dry]" /><label>Wet</label><input id="breakfast_wet" type="text" name="feeding[breakfast_wet]" /><br/>
		    <label>Supper</label>Dry<input id="supper_dry" type="text" name="feeding[supper_dry]" /><label>Wet</label><input id="supper_wet" type="text" name="feeding[supper_wet]" /><br/>
		     	
		</div>
		<input type="submit" value="Save" />
	</div>
</form>
<?php include(SITE::$TEMPLATEPATH."/meta/footer.php"); ?>