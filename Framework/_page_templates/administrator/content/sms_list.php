<?php include(SITE::$TEMPLATEPATH."/meta/header.php"); ?>
<?php 
	if(!$DataStore->auth->auth('admin')):
		header('location:'.SITE::$PATH.'/administrator/login');
	endif;
	
	$results = $DataStore->sms->get_data(); 
	pre($_POST);
?>

<div>
	<table id="myTable" class="tablesorter sortable"> 
		<thead>
		<tr>
			<th>Title</th>
			<th>Modified Date</th>
			<th>Edited By</th>
		</tr>
		</thead>
		<tbody>
			<?php foreach($results as $result): ?>
				<tr id="<?php echo $result['id']; ?>" onclick="window.location = '<?php echo SITE::$PATH ?>/administrator/content/sms/<?php echo $result['id']; ?>' ">
					<td><?php echo $result['title']; ?></td>
					<td><?php echo $result['date_modified']; ?></td>
					<td><?php echo $result['email_address']; ?></td>
				</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
	
</div>


This is the template list for smses


<?php include(SITE::$TEMPLATEPATH."/meta/footer.php"); ?>