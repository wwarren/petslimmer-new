var AXL = function(){ this.init(); };
AXL.prototype.conf = {formhandler : '/_formhandler'};
AXL.prototype.init = function(){
	this.conf.formhandler = $("#SITEROOT").val()+this.conf.formhandler;
	this._buttonBootstrap();
	this._formBootstrap();
	this._updateBootstrap();
	this._modalBootstrap();
	};
AXL.prototype.sendData = function(formDataSerialized,FormLogic,JsCallBack,Element){
	formDataSerialized.push({name : "formlogic", value : "axl_handlers/"+FormLogic});
	if(JsCallBack != ""){
		$.ajax({
			type: "POST",
			url: this.conf.formhandler,
			data: formDataSerialized,
			success: function(returnData , returnText, jqXHR ){
				eval(JsCallBack);
			}
		});
	}else{
		$.ajax({
			type: "POST",
			url: this.conf.formhandler,
			data: formDataSerialized
		});
	}
};
AXL.prototype._modalBootstrap = function(){
	var AxlSelector = ".axlModal"; var AxlFrame = this;
	$(document).on(
		"mouseup",
		AxlSelector,
		function(event){
			event.preventDefault();
			var RawFormData = $(":input",this).serializeArray();
			formData = []; target = '';	callback = '';
			$(RawFormData).each(function(ValueIndex,ValueObj){
				if(ValueObj.name == 'axl_target'){ target = ValueObj.value;	}
				else { formData.push({ "name" : ValueObj.name,"value" : ValueObj.value }); }
			});
			$('#modalContent *').remove();
			$('#modalContent').html("Loading...");
			AxlFrame.sendData(formData,target,AxlFrame._showModal,this);
		}
	);
};
AXL.prototype._showModal = function(data){
	if($('#modalContent').length == 0){
		// Create Modal Frame
		var ModalFrame = $("<div class='row container modal'><div class='modal-toolbar'>Close</div><div id='modalContent'></div></div>").attr('id','axlModalFrame');
		ModalFrame.css({
		  position: "absolute",
		  'z-index': "200",
		  margin: "5% auto",
		  left: "0",
		  right: "0",
		  top: "0"
		});
		var ScreenGaurd = $("<div class='screengaurd'></div>");
		$('html').append(ScreenGaurd);
		$('html').append(ModalFrame);
		$('.modal-toolbar').on("click",function(event){
				event.preventDefault();
				$('.modal').remove();
				$('.screengaurd').remove();
		})
	}
	$('#modalContent').html(data);
};
AXL.prototype._buttonBootstrap = function(){
	var AxlSelector = ".axlButton"; var AxlFrame = this;

	$(document).on(
		"mouseup",
		AxlSelector,
		function(event){
			event.preventDefault();
			var RawFormData = $(":input",this).serializeArray();
			formData = []; target = '';	callback = '';
			$(RawFormData).each(function(ValueIndex,ValueObj){
				if(ValueObj.name == 'axl_target'){ target = ValueObj.value;	}
				else if(ValueObj.name == 'axl_callback'){	callback = ValueObj.value;}
				else { formData.push({ "name" : ValueObj.name,"value" : ValueObj.value }); }
			});
			AxlFrame.sendData(formData,target,callback,this);
		}
	);
};
AXL.prototype._formBootstrap = function(){
	var AxlSelector = ".axlForm"; var AxlFrame = this;

	$(document).on(
		"submit",
		AxlSelector,
		function(event){
			event.preventDefault();
			var RawFormData = $(":input",this).serializeArray();
			formData = []; target = '';	callback = '';
			$(RawFormData).each(function(ValueIndex,ValueObj){
				if(ValueObj.name == 'axl_target'){ target = ValueObj.value;	}
				else if(ValueObj.name == 'axl_callback'){	callback = ValueObj.value;}
				else { formData.push({ "name" : ValueObj.name,"value" : ValueObj.value }); }
			});
			AxlFrame.sendData(formData,target,callback,this);
		}
	);
};
AXL.prototype._updateTriggers = function(){
	$(axl_updateTriggerObjs).each(function(elid,elObjDetails){

		if(axl_updateTickerTicks === 0 || (axl_updateTickerTicks * 10) % elObjDetails.trigger === 0){
			elObjDetails.ticks++;
			if(elObjDetails.ticks == 1 && elObjDetails.trigger == undefined){
				$("#"+elObjDetails.element_id).trigger("axl_update");
			}else if(elObjDetails.trigger != undefined){
				$("#"+elObjDetails.element_id).trigger("axl_update");
			}
		}
	});
	axl_updateTickerTicks = axl_updateTickerTicks+1;
}
var axl_updateTickerTicks = 0;
var axl_updateTriggerObjs = [];
AXL.prototype._updateBootstrap = function(){
	this._updateTicker = setInterval(this._updateTriggers, 10);
	var AxlSelector = ".axlUpdate"; var AxlFrame = this;
	$(AxlSelector).each(function(ElIndex,ElObj){
		if($(ElObj).attr('axl_ticker_trigger') == undefined){ $(ElObj).attr('axl_ticker_trigger',$("input[name='axl_ticker_wait']",$(ElObj)).val()) }

		if($(ElObj).attr('axl_ticker_val') == undefined){ $(ElObj).attr('axl_ticker_val',0) }
		$(ElObj).on("axl_update",function(event){
			event.preventDefault();
			var RawFormData = $(":input",this).serializeArray();
			formData = []; target = '';	callback = '';
			$(RawFormData).each(function(ValueIndex,ValueObj){
				if(ValueObj.name == 'axl_target'){ target = ValueObj.value;	}
				else if(ValueObj.name == 'axl_callback'){	callback = ValueObj.value;}
				else if(ValueObj.name == 'axl_ticker_wait'){ }
				else { formData.push({ "name" : ValueObj.name,"value" : ValueObj.value }); }
			});
			AxlFrame.sendData(formData,target,callback,this);
		});
		axl_updateTriggerObjs.push(
			{
				element_id : $(ElObj).attr('id'),
				trigger : parseInt($(ElObj).attr('axl_ticker_trigger')),
				ticks : 0
			}
		);
	});
};
var SetModalContent = function(contentHTML){
	$('#modalContent *').remove();
	$('#modalContent').html("Loading...");
	if($('#modalContent').length == 0){
		return false;
	}else{
		if(typeof(contentHTML) != undefined){
			$('#modalContent').html(contentHTML);
		}
		return $('#modalContent').html();
	}
}
