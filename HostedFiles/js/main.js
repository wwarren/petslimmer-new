$(function(){
    /* FORMS SETUP workaround */
    $(document).on('submit','form',function(Event){

        var passwordFields = $("input[type='password']",this).clone();
        $("input[type='password']",this).attr('name','');
        $("input[type='password']",this).attr('readonly',true);
        passwordFields.attr('type','hidden');
        passwordFields.each(function(InputIndex,InputElement){
            // SALTING automaticall happens here
            $(InputElement).val(CryptoJS.MD5($(InputElement).val()));
        });
        $(this).prepend(passwordFields);
        $(this).prepend($('<input type="hidden" value="'+$(this).attr('action')+'" name="formlogic" />'));
        $(this).attr('method','post');
        $(this).attr('action',$("#SITEROOT").val()+"/_formhandler");
        $(this).attr('enctype','multipart/form-data');
        return true;
    });
    /* Drop down menu functions */
  
    var axlSystem = new AXL();
    
    
    
    /* Table Sorting functions */
    $(".sortable").tablesorter(); 
});
